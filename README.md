# Design Repository

This repository contains a collection of graphics for web instances and printing, like FabCards or stickers.

You will find more information about FabAccess Corporate Identity at [Corporate Identity](https://docs.fab-access.org/books/corporate-identity)